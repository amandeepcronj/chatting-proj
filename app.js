var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require("socket.io")(server);	

var users = []; //contains array of object(username,socketId)
var roomno = 0;

app.use(express.static('public'));

app.get("/", function(req,res) {
	res.sendFile(__dirname+'/index.html');
});

server.listen(3000,function() {
	console.log("Listen to : "+3000);
});

io.on('connection', function(socket) {
	socket.on("message",function(data) {

		console.log(data);
		socket.emit("message",data);
	});
	

	socket.on("addUser", function(username,callback) {
		if(isDataExist(users,"username",username)) {
			var error = "username already exist";
			callback(error);
		} else {
			users.push({username : username, socketId : socket.id});
			callback(null,{socketId : socket.id});
		}
	})

	socket.on("getUsersList", function(data) {
		io.emit("usersList",users);
	});

	socket.on("chatMessage", function(data) { //data contain object(message,destination)
		io.to(data.destination).emit("privateMsg",{message : data.message, sender : socket.id});
	});

	socket.on("disconnect", function() {
		if(isDataExist(users,"socketId",socket.id)) {
			var index = getSocketIndex(socket.id);
			console.log(index);
			var obj = users.splice(index,1);
			io.emit("usersList",users);
			console.log("removed : "+ obj[0].username+obj[0].socketId);
		};

		console.log("diconnected");
	})

	socket.on("createRoom",function(data) {
		var roomName = "room"+roomno;
		roomno++;
		socket.join(roomName);
	})
});

function isUsernameExist(username) {
	if(users.length) {
		var bool = false;
		users.forEach(function(user) {
			if(user.username === username) {
				bool = true;
				return;
			}
		});
		return bool;
	} 
	else {
		return false;
	}
}
/*
*To check particular data exist in the array of object (for particular object attribute)
* @params
* array : array of objects
* objAttribute : the attribute of object, where to compare your data
* data : the data you want to compare
*/
function isDataExist(array,objAttribute,data) {
	if(array.length) {
		var bool = false;
		array.forEach(function(object) {
			if(object[objAttribute] === data) {
				bool = true;
				return;
			}
		});
		return bool;
	} 
	else {
		return false;
	}
}

function getSocketIndex(socketId) {
	for(var i=0;i<users.length;i++) {
		if(users[i].socketId == socketId) {
			return i;
		}
	}
	return -1;
}



function getSocket(socketId) {
	return io.sockets.connected[socketId];
}





